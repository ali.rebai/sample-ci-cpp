FROM ubuntu:16.04

LABEL NAME="sample-ci-cpp" \
      VERSION="16.04" \
      DESC="Ubuntu 16.04 with added packages to support CI tests." \
      PACKAGES="cmake,g++,git,lcov,pthread"

RUN apt-get update --yes && \
    apt-get install --yes g++ lcov cmake git libpthread-stubs0-dev make

# Set the working directory inside the Docker image
WORKDIR /sample-ci-cpp

# Copy everything
COPY . /sample-ci-cpp

# Build the app and tests
RUN rm -Rf build && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j8
